package pl.eduweb.calculator;

import android.graphics.Path;

public enum Operation {

    NONE(""), ADD("+"), SUBSTRACT("-"), MULTIPLY( "*"), DIVIDE( "/");

    private final String key;

    private Operation(String key){
        this.key = key;
    }

    public static Operation OperationFromKey(String key){
        for (Operation operation : values()) {
            if(operation.key.equals(key)){
                return operation;
            }
        } return NONE;


    }
}
